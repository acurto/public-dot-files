#################### GENERAL VARIABLES ####################
USER="agustincurto"


#################### Exports ####################
# If you come from bash you might have to change your $PATH.
export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH=/Users/$USER/.oh-my-zsh

export PATH="/usr/local/opt/ncurses/bin:$PATH"

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes

#################### OhMyZSH Configurations ####################
ZSH_THEME="acurto"

# Set list of themes to load
# Setting this variable when ZSH_THEME=random
# cause zsh load theme from this variable instead of
# looking in ~/.oh-my-zsh/themes/
# An empty array have no effect
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
  git
)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"


#################### Personal aliases ####################

# Developer aliases
alias dev="cd /Users/$USER/Developer"
alias brewall="brew update && brew upgrade && brew cleanup && brew doctor"
alias jn="jupyter notebook"

# Degree aliases
alias degree="cd /Users/agustincurto/Documents/faculty/degree"
alias comp="cd /Users/agustincurto/Documents/faculty/degree/fifth/compiladores"
alias thesis="cd /Users/agustincurto/Documents/faculty/degree/thesis"
alias uvlm="cd /Users/agustincurto/Documents/faculty/degree/thesis/uvlm"
alias run="python3 -O check_results.py"
alias debug="python3 check_results.py"

############### Teaching aliases ###############
alias teach="cd /Users/agustincurto/Documents/faculty/teaching"

# Other aliases
alias rmpermissions1="sudo xattr -rc"
alias rmpermissions2="sudo chmod -R -N"
alias untar="tar -xvzf"


######################### Clusters access aliases #########################
alias zx81="cp201806@zx81.famaf.unc.edu.ar"
alias ssh-zx81="ssh cp201806@zx81.famaf.unc.edu.ar"

alias mendieta="acurto@mendieta.ccad.unc.edu.ar"
alias ssh-mendieta="ssh acurto@mendieta.ccad.unc.edu.ar"

alias eulogia="acurto@eulogia.ccad.unc.edu.ar"
alias ssh-eulogia="ssh acurto@eulogia.ccad.unc.edu.ar"

alias mulatona="acurto@mulatona.ccad.unc.edu.ar"
alias ssh-mulatona="ssh acurto@mulatona.ccad.unc.edu.ar"
